//
//  ViewController.swift
//  AppA
//
//  Created by Akhil Balan on 2/13/18.
//  Copyright © 2018 Akhil. All rights reserved.
//

import UIKit
import FrameworkB

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let test = TestB()
        test.methodB()
    }

}

